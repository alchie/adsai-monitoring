<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System_modules extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Module Settings');
		$this->template_data->set('current_uri', 'system_modules');

		$this->_isAuth('system', 'modules', 'view');

	}

	public function index($start=0) {
		$users = new $this->Modules_settings_model;
		$users->set_start($start);
		$this->template_data->set('users', $users->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => base_url($this->config->item("index_page") . '/system_users/index'),
			'total_rows' => $users->count_all_results(),
			'per_page' => $users->get_limit(),
			"ajax" => true,
		)));

		$this->load->view('system/modules/modules', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('system', 'users', 'add');

		if( $this->input->post() ) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[modules.username]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required|matches[password]');
			if( $this->form_validation->run() ) {
				$user = new $this->Modules_settings_model;
				$user->setName($this->input->post('full_name'));
				$user->setUsername($this->input->post('username'),true);
				$user->setPassword(sha1($this->input->post('password')));
				if( ! $user->nonEmpty() ) {
					$user->insert();
					redirect(site_url("system_users") . "?success=true" );
				} else {
					redirect(site_url("system_users/add") . "?error=true" );
				}
			}
		}

		$this->template_data->set('output', $output);

		$this->load->view('system/modules/modules_add', $this->template_data->get_data());
	}

	public function edit($id, $output='') {

		$this->_isAuth('system', 'users', 'edit');

		$user = new $this->Modules_settings_model;
		$user->setId($id, true);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required');

			if( $this->input->post('password') ) {
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required|matches[password]');
			}
			if( $this->form_validation->run() ) {
				$user->setName($this->input->post('full_name'), false, true);
				$user->setUsername($this->input->post('username'), false, true);
				if( $this->input->post('password') ) {
					$user->setPassword(sha1($this->input->post('password')), false, true);
				} 
				if( $user->nonEmpty() ) {
					$user->set_exclude('id');
					$user->update();
				} 
			}

			if( $this->input->get('next') ) {
				redirect( $this->input->get('next') . "?success=true" );
			} else {
				redirect( site_url("system_users") . "?success=true" );
			}
		}
		$this->template_data->set('user', $user->get());

		$this->template_data->set('output', $output);

		$this->load->view('system/modules/modules_edit', $this->template_data->get_data());
	}

	public function delete($id) {

		$this->_isAuth('system', 'users', 'delete');

		if($this->session->user_id != $id) {
			$user = new $this->Modules_settings_model;
			$user->setId($id, true);
			$user->delete();
			redirect(site_url("system_users") . "?success=true" );
		}
		redirect(site_url("system_users") . "?error=true" );
	}

}
