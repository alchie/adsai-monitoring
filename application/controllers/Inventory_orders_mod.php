<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Inventory_orders Controller Class
 *
 * Manipulates `inventory_orders` table on database

 * @package			        Controller
 * @version_number	        1.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.0.1
 */
 
class Inventory_orders_mod extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Inventory Orders');
		$this->template_data->set('current_uri', 'inventory_orders');
		
		$this->_isAuth('modules', 'inventory_orders', 'view');

	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$index_data = new $this->Inventory_orders_model('d');
		$index_data->set_select("d.*");
		$index_data->set_start($start);
		$this->template_data->set('index_datas', $index_data->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/inventory_orders/index/'),
			'total_rows' => $index_data->count_all_results(),
			'per_page' => $index_data->get_limit()
		)));

		$this->load->view('modules/inventory_orders/inventory_orders_list', $this->template_data->get_data());
	}

}
