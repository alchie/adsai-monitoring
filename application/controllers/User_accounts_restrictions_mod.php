<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User_accounts_restrictions Controller Class
 *
 * Manipulates `user_accounts_restrictions` table on database

 * @package			        Controller
 * @version_number	        1.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.0.1
 */
 
class User_accounts_restrictions_mod extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'User Accounts Restrictions');
		$this->template_data->set('current_uri', 'user_accounts_restrictions');
		
		$this->_isAuth('modules', 'user_accounts_restrictions', 'view');

	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$index_data = new $this->User_accounts_restrictions_model('d');
		$index_data->set_select("d.*");
		$index_data->set_start($start);
		$this->template_data->set('index_datas', $index_data->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/user_accounts_restrictions/index/'),
			'total_rows' => $index_data->count_all_results(),
			'per_page' => $index_data->get_limit()
		)));

		$this->load->view('modules/user_accounts_restrictions/user_accounts_restrictions_list', $this->template_data->get_data());
	}

}
