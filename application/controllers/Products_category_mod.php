<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Products_category Controller Class
 *
 * Manipulates `products_category` table on database

 * @package			        Controller
 * @version_number	        1.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.0.1
 */
 
class Products_category_mod extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Products Category');
		$this->template_data->set('current_uri', 'products_category');
		
		$this->_isAuth('modules', 'products_category', 'view');

	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$index_data = new $this->Products_category_model('d');
		$index_data->set_select("d.*");
		$index_data->set_start($start);
		$this->template_data->set('index_datas', $index_data->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/products_category/index/'),
			'total_rows' => $index_data->count_all_results(),
			'per_page' => $index_data->get_limit()
		)));

		$this->load->view('modules/products_category/products_category_list', $this->template_data->get_data());
	}

}
