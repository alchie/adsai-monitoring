<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Account_sessions Controller Class
 *
 * Manipulates `account_sessions` table on database

 * @package			        Controller
 * @version_number	        1.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.0.1
 */
 
class Account_sessions_mod extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Account Sessions');
		$this->template_data->set('current_uri', 'account_sessions');
		
		$this->_isAuth('modules', 'account_sessions', 'view');

	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$index_data = new $this->Account_sessions_model('d');
		$index_data->set_select("d.*");
		$index_data->set_start($start);
		$this->template_data->set('index_datas', $index_data->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/account_sessions/index/'),
			'total_rows' => $index_data->count_all_results(),
			'per_page' => $index_data->get_limit()
		)));

		$this->load->view('modules/account_sessions/account_sessions_list', $this->template_data->get_data());
	}

}
