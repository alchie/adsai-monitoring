<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Modules_settings_model Class
 *
 * Manipulates `modules_settings` table on database

CREATE TABLE `modules_settings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `table` varchar(200) NOT NULL,
  `field` varchar(200) NOT NULL,
  `option_key` varchar(200) NOT NULL,
  `option_value` text,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `modules_settings` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `modules_settings` ADD  `table` varchar(200) NOT NULL   ;
 ALTER TABLE  `modules_settings` ADD  `field` varchar(200) NOT NULL   ;
 ALTER TABLE  `modules_settings` ADD  `option_key` varchar(200) NOT NULL   ;
 ALTER TABLE  `modules_settings` ADD  `option_value` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Modules_settings_model extends MY_Model {

	protected $id;
	protected $table;
	protected $field;
	protected $option_key;
	protected $option_value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'modules_settings';
		$this->_short_name = 'modules_settings';
		$this->_fields = array("id","table","field","option_key","option_value");
		$this->_required = array("table","field","option_key");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: table -------------------------------------- 

	/** 
	* Sets a value to `table` variable
	* @access public
	*/

		public function setTable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('table', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `table` variable
	* @access public
	*/

		public function getTable() {
			return $this->table;
		}
	
// ------------------------------ End Field: table --------------------------------------


// ---------------------------- Start Field: field -------------------------------------- 

	/** 
	* Sets a value to `field` variable
	* @access public
	*/

		public function setField($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('field', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `field` variable
	* @access public
	*/

		public function getField() {
			return $this->field;
		}
	
// ------------------------------ End Field: field --------------------------------------


// ---------------------------- Start Field: option_key -------------------------------------- 

	/** 
	* Sets a value to `option_key` variable
	* @access public
	*/

		public function setOptionKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('option_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `option_key` variable
	* @access public
	*/

		public function getOptionKey() {
			return $this->option_key;
		}
	
// ------------------------------ End Field: option_key --------------------------------------


// ---------------------------- Start Field: option_value -------------------------------------- 

	/** 
	* Sets a value to `option_value` variable
	* @access public
	*/

		public function setOptionValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('option_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `option_value` variable
	* @access public
	*/

		public function getOptionValue() {
			return $this->option_value;
		}
	
// ------------------------------ End Field: option_value --------------------------------------




}

/* End of file Modules_settings_model.php */
/* Location: ./application/models/Modules_settings_model.php */
