<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('system/system_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Restrictions : <strong><?php echo $user->name; ?></strong></h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  
<?php 
$rest_depts = array();
$rest = array();
foreach($user_restrictions as $ur) {
    $rest[$ur->department][$ur->section]['view'] = $ur->view;
    $rest[$ur->department][$ur->section]['add'] = $ur->add;
    $rest[$ur->department][$ur->section]['edit'] = $ur->edit;
    $rest[$ur->department][$ur->section]['delete'] = $ur->delete;

    if( $ur->view || $ur->add || $ur->edit || $ur->delete ) {
        $rest_depts[$ur->department] = 1;
    }
}
function panel_item($id, $dep, $rest, $rest_depts) {
$panel_color = (isset($rest_depts[$id])) ? 'success' : 'default';
echo <<<HTML
  <div class="panel panel-{$panel_color}">
    <div class="panel-heading" role="tab" id="heading-{$id}">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$id}" aria-expanded="true" aria-controls="collapseOne">
          {$dep->title}
        </a>
      </h4>
    </div>
    <div id="collapse-{$id}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{$id}; ?>">
      <div class="panel-body">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Section</th>
                <th class="text-center" width="5%">View</th>
                <th class="text-center" width="5%">Add</th>
                <th class="text-center" width="5%">Edit</th>
                <th class="text-center" width="5%">Delete</th>
            </tr>
            </thead>
            <tbody>
HTML;
            foreach($dep->sections as $sect_id=>$sect_name) { 
                $view_checked = (isset($rest[$id][$sect_id]['view']) && $rest[$id][$sect_id]['view']) ? 'CHECKED' : '';
                $add_checked = (isset($rest[$id][$sect_id]['add']) && $rest[$id][$sect_id]['add']) ? 'CHECKED' : '';
                $edit_checked = (isset($rest[$id][$sect_id]['edit']) && $rest[$id][$sect_id]['edit']) ? 'CHECKED' : '';
                $delete_checked = (isset($rest[$id][$sect_id]['delete']) && $rest[$id][$sect_id]['delete']) ? 'CHECKED' : '';
echo <<<HTML
            <tr>
                <td>{$sect_name['title']}</td>
                <td class="text-center"><input {$view_checked} type="checkbox" name="r[{$id}][{$sect_id}][view]" value="1"></td>
                <td class="text-center"><input {$add_checked} type="checkbox" name="r[{$id}][{$sect_id}][add]" value="1"></td>
                <td class="text-center"><input {$edit_checked} type="checkbox" name="r[{$id}][{$sect_id}][edit]" value="1"></td>
                <td class="text-center"><input {$delete_checked} type="checkbox" name="r[{$id}][{$sect_id}][delete]" value="1"></td>
            </tr>
HTML;
 }
echo <<<HTML
            </tbody>
        </table>
      </div>
    </div>
  </div>
HTML;
}
$dept = unserialize(USERACCOUNTS_RESTRICTIONS);
$dept['modules'] = (object) array(
    'title' => 'Modules',
    'sections'=> $this->config->item('modules')
);
foreach($dept as $id=>$dep) { 
    panel_item( $id, $dep, $rest, $rest_depts );
 } ?>

</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("system_users"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>