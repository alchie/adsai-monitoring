<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('modules/modules_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('modules', 'account_sessions', 'add') ) { ?>
	<button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Account Sessions" data-url="<?php echo site_url("account_sessions_mod/add/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Add Account Sessions</button>
<?php } ?>
	    		<h3 class="panel-title">Account Sessions</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<?php if( $index_datas ) { ?>

	    		<table class="table table-default">
	    			<thead>
	    				<tr>
	    					<th>Full Name</th>
	    					<th>Username</th>
	    					<?php if( hasAccess('modules', 'account_sessions', 'edit') ) { ?>
	    						<th width="140px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($index_datas as $index_data) { ?>
	    				<tr>
	    					<td><?php echo $index_data->name; ?></td>
	    					<td><?php echo $index_data->username; ?></td>
	    				<?php if( hasAccess('modules', 'account_sessions', 'edit') ) { ?>
	    					<td>
	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Account Sessions : <?php echo $index_data->name; ?>" data-url="<?php echo site_url("account_sessions_mod/edit/{$index_data->id}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Edit</button>
	    					</td>
	    				<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No User Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>
