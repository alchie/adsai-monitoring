<?php

$modules = array();

$modules['account_sessions'] = array(
      'title' => 'Account Sessions',
      'uri' => 'account_sessions_mod',
      'permission' => 'account_sessions',
      'fields' => array("id","ip_address","timestamp","data"),
      'required_fields' => array("id","ip_address","timestamp","data"),
      'primary_field' => 'id'
      );

$modules['inventory_orders'] = array(
      'title' => 'Inventory Orders',
      'uri' => 'inventory_orders_mod',
      'permission' => 'inventory_orders',
      'fields' => array("id","date_order","requester","remarks"),
      'required_fields' => array("date_order"),
      'primary_field' => 'id'
      );

$modules['inventory_purchases'] = array(
      'title' => 'Inventory Purchases',
      'uri' => 'inventory_purchases_mod',
      'permission' => 'inventory_purchases',
      'fields' => array("id","order_id","date_purchase","purchaser","remarks"),
      'required_fields' => array("date_purchase"),
      'primary_field' => 'id'
      );

$modules['inventory_stocks'] = array(
      'title' => 'Inventory Stocks',
      'uri' => 'inventory_stocks_mod',
      'permission' => 'inventory_stocks',
      'fields' => array("id","item_id","conn_id","type","quantity","price","receipt_id","item_date","content"),
      'required_fields' => array("item_id","conn_id","quantity","price","item_date"),
      'primary_field' => 'id'
      );

$modules['lists_buildings'] = array(
      'title' => 'Lists Buildings',
      'uri' => 'lists_buildings_mod',
      'permission' => 'lists_buildings',
      'fields' => array("id","building_name","address","contact_number"),
      'required_fields' => array("building_name"),
      'primary_field' => 'id'
      );

$modules['lists_names'] = array(
      'title' => 'Lists Names',
      'uri' => 'lists_names_mod',
      'permission' => 'lists_names',
      'fields' => array("id","full_name","address","contact_number"),
      'required_fields' => array("full_name"),
      'primary_field' => 'id'
      );

$modules['products_category'] = array(
      'title' => 'Products Category',
      'uri' => 'products_category_mod',
      'permission' => 'products_category',
      'fields' => array("id","name","trolley","active"),
      'required_fields' => array("name"),
      'primary_field' => 'id'
      );

$modules['products_items'] = array(
      'title' => 'Products Items',
      'uri' => 'products_items_mod',
      'permission' => 'products_items',
      'fields' => array("item_id","start","item_name","net_weight","content","category_id","store_id","minimum","active","shelf"),
      'required_fields' => array("start","item_name","content"),
      'primary_field' => 'item_id'
      );

$modules['products_store'] = array(
      'title' => 'Products Store',
      'uri' => 'products_store_mod',
      'permission' => 'products_store',
      'fields' => array("id","name","active"),
      'required_fields' => array("name"),
      'primary_field' => 'id'
      );

$modules['user_accounts'] = array(
      'title' => 'User Accounts',
      'uri' => 'user_accounts_mod',
      'permission' => 'user_accounts',
      'fields' => array("id","username","password","name"),
      'required_fields' => array("username","password","name"),
      'primary_field' => 'id'
      );

$modules['user_accounts_restrictions'] = array(
      'title' => 'User Accounts Restrictions',
      'uri' => 'user_accounts_restrictions_mod',
      'permission' => 'user_accounts_restrictions',
      'fields' => array("uid","department","section","view","add","edit","delete"),
      'required_fields' => array("uid","department","section","view","add","edit","delete"),
      'primary_field' => 'id'
      );

$config['modules'] = $modules;